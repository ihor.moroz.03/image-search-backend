﻿using System.Text.Json.Serialization;

namespace ImageSearchAPI.Data
{
    public class UserEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Username { get; set; }

        [JsonIgnore]
        public string Password { get; set; }

        public bool IsAdmin { get; set; } = false;
    }
}
