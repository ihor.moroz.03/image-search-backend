﻿namespace ImageSearchAPI.Data
{
    public class ImageEntity
    {
        public int Id { get; set; }

        public string Title { get; set; } = "Untitled";

        public string? URI { get; set; }

        public UserEntity? User { get; set; }
    }
}
