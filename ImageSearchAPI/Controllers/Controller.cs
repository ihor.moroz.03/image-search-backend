﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ImageSearchAPI.Data;

namespace ImageSearchAPI.Controllers
{
    [Route("/")]
    [ApiController]
    public class Controller : ControllerBase
    {
        private static readonly Dictionary<UserEntity, DateTime> _signedIn = new();
        private readonly DatabaseContext _context;

        static Controller()
        {
            ThreadPool.QueueUserWorkItem(state =>
            {
                while (true)
                {
                    var toRemove = _signedIn
                        .Where(pair => (DateTime.Now - pair.Value).TotalMinutes > 5)
                        .Select(pair => pair.Key);

                    foreach (var user in toRemove)
                        _signedIn.Remove(user);

                    Thread.Sleep(10000);
                }
            });
        }

        public Controller(DatabaseContext context)
        {
            _context = context;
        }

        [HttpPost("users/login")]
        public async Task<ActionResult<int>> SignIn(UserSignIn userEntity)
        {
            if (_context.Users == null)
            {
                return Problem("Entity set 'DatabaseContext.Users'  is null.");
            }

            UserEntity? user = await _context.Users.FirstOrDefaultAsync(
                entity =>
                entity.Username == userEntity.Username &&
                entity.Password == userEntity.Password
                );

            if (user == null) return Problem("User does not exist");

            _signedIn[user] = DateTime.Now;
            return Ok(new { Token = GenerateToken(user) });
        }

        [HttpPost("users/register")]
        public async Task<ActionResult<int>> SignUp(UserSignUp user)
        {
            if (_context.Users == null)
            {
                return Problem("Entity set 'DatabaseContext.Users'  is null.");
            }

            if (user == null) return BadRequest();

            UserEntity newUser = new()
            {
                Username = user.Username,
                Password = user.Password,
                Name = user.Name
            };

            if (_context.Users.FirstOrDefault(user => user.Username == newUser.Username) != null)
                return Problem("User with such username already exists");

            _context.Users.Add(newUser);
            await _context.SaveChangesAsync();

            _signedIn[newUser] = DateTime.Now;
            return Ok(new { Token = GenerateToken(newUser) });
        }

        [HttpPost("images")]
        public async Task<ActionResult<ImageEntity>> PostImageEntity(MyRequest request)
        {
            if (_context.Images == null)
            {
                return Problem("Entity set 'DatabaseContext.Images'  is null.");
            }

            UserEntity? user = GetUserByToken(request.Token);
            if (user == null || !user.IsAdmin)
                return Problem("Access denied. Sign in as admin, please");

            if (request.Image == null)
                return Problem("Image is null");

            ImageEntity imageEntity = new()
            {
                //Id = Random.Shared.Next(500),
                Title = request.Image.Title,
                URI = request.Image.URI,
                User = user,
            };
            _context.Images.Add(imageEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetImageEntity", new { id = imageEntity.Id }, imageEntity);
        }

        [HttpPut("images/{id}")]
        public async Task<IActionResult> PutImageEntity(int id, MyRequest request)
        {
            UserEntity? user = GetUserByToken(request.Token);
            if (user == null || !user.IsAdmin)
                return Problem("Access denied. Sign in as admin, please");

            if (request.Image == null)
                return Problem("Image is null");

            ImageEntity imageEntity = new()
            {
                Id = request.Image.Id,
                Title = request.Image.Title,
                URI = request.Image.URI,
            };

            if (id != imageEntity.Id)
            {
                return BadRequest();
            }

            _context.Entry(imageEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImageEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpDelete("images/{id}")]
        public async Task<IActionResult> DeleteImageEntity(int id, MyRequest request)
        {
            UserEntity? user = GetUserByToken(request.Token);
            if (user == null || !user.IsAdmin)
                return Problem("Access denied. Sign in as admin, please");

            if (_context.Images == null)
            {
                return NotFound();
            }
            var imageEntity = await _context.Images.FindAsync(id);
            if (imageEntity == null)
            {
                return NotFound();
            }

            _context.Images.Remove(imageEntity);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpGet("images")]
        public async Task<ActionResult<IEnumerable<ImageEntity>>> GetImages()
        {
            if (_context.Images == null)
            {
                return NotFound();
            }
            return await _context.Images.ToListAsync();
        }

        // GET: api/ImageEntities/5
        [HttpGet("images/{id}")]
        public async Task<ActionResult<ImageEntity>> GetImageEntity(int id)
        {
            if (_context.Images == null)
            {
                return NotFound();
            }
            var imageEntity = await _context.Images.FindAsync(id);

            if (imageEntity == null)
            {
                return NotFound();
            }

            return imageEntity;
        }

        [HttpGet("images/query/{query}")]
        public async Task<ActionResult<IEnumerable<ImageEntity>>> GetImages(string query)
        {
            if (_context.Images == null)
            {
                return NotFound();
            }

            return await _context.Images
                .Where(image => image.Title.ToLower().Contains(query.ToLower()))
                .ToListAsync();
        }

        [HttpGet("users/{token}")]
        public async Task<ActionResult<Status>> GetStatus(int token)
        {
            UserEntity? user = await Task.Run(() => GetUserByToken(token));
            return new Status()
            {
                SignedIn = user != null,
                IsAdmin = user?.IsAdmin ?? false,
                Username = user?.Username
            };
        }

        int GenerateToken(UserEntity user)
        {
            return HashCode.Combine(user.Username, user.Password);
        }

        UserEntity? GetUserByToken(int token)
        {
            UserEntity? user = _signedIn.Keys.FirstOrDefault(user => GenerateToken(user) == token);
            if (user != null) user = _context.Users.First(u => u.Username == user.Username);
            return user;
        }

        bool ImageEntityExists(int id)
        {
            return (_context.Images?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        public class UserSignIn
        {
            public string Username { get; set; } = "";
            public string Password { get; set; } = "";
        }

        public class UserSignUp : UserSignIn
        {
            public string Name { get; set; } = "n/a";
        }

        public class MyRequest
        {
            public int Token { get; set; }
            public ImageRequest? Image { get; set; }
        }

        public class ImageRequest
        {
            public int Id { get; set; }
            public string Title { get; set; } = "Untitled";
            public string? URI { get; set; }
        }

        public class Status
        {
            public bool SignedIn { get; set; }
            public bool IsAdmin { get; set; }
            public string? Username { get; set; }
        }
    }
}
